import Statement from '../src/statement.js';
import plays from '../fixtures/plays.json'
import invoices from '../fixtures/invoices.json'

test('returns expected output from fixtures', () => {
  const expectedOutput = `Statement for BigCo
  Hamlet: $650.00 (55 seats)
  As You Like It: $580.00 (35 seats)
  Othello: $500.00 (40 seats)
Amount owed is $1,730.00
You earned 47 credits
`;
  expect(Statement(invoices[0], plays)).toBe(expectedOutput);
});