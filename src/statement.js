/*
    Image a company of theatrical players who go out to various events performing
    plays. Typically, a customer will request a few plays and the company charges
    them based on the size of the audience and the kind of play they perform.
    There are currently two kinds of plays that company performs: tragedies and comedies.
    As well as providing a bill for the performance, the company gives its customers
    “volume credits” which they can use for discounts on future performances—think of
    it as a customer loyalty mechanism.
 */

import createStatementData from './createStatementData';

export default function statement (invoice, plays) {
  return renderPlainText(createStatementData(invoice, plays));
}

function renderPlainText(data) {
    let result = `Statement for ${data.customer}\n`;

    for (let perf of data.performances) {
      //print line for this order
      result += `  ${perf.play.name}: ${usd(perf.amount)} (${perf.audience} seats)\n`;
    }

    result += `Amount owed is ${usd(data.totalAmount)}\n`;
    result += `You earned ${data.totalVolumeCredits} credits\n`;
    return result;

  function usd(aNumber) {
    return new Intl.NumberFormat("en-US", {
      style: "currency", currency: "USD",
      minimumFractionDigits: 2
    }).format(aNumber/100);
  }
}